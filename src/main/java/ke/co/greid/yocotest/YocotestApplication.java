package ke.co.greid.yocotest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ke.co.greid.yocotest.model.GameModel;
import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class YocotestApplication {
    private static final Logger log = LoggerFactory.getLogger(YocotestApplication.class);
    private static List<GameModel> gameModels = new ArrayList<>();

    public static void main(String[] args) {
        SpringApplication.run(YocotestApplication.class, args);

        RestTemplate restTemplate = new RestTemplate();
        String url = "https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json";
        String jsonArray = restTemplate.getForObject(url, String.class);

        JSONArray objects = new JSONArray(jsonArray);
        System.out.println(objects.length());

        for (int i = 0; i < objects.length(); i++) {
            JSONObject jsonObject = objects.getJSONObject(i);
            ObjectMapper mapper = new ObjectMapper();

            try {
                GameModel gameModel = mapper.readValue(jsonObject.toString(), GameModel.class);
//				System.out.println(gameModel);
                gameModels.add(gameModel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (GameModel gameModel : gameModels) {
            evaluateWinner(gameModel);

        }


    }


    private static void evaluateWinner(GameModel gameModel) {
        String winner = "";
        String[] playerA = gameModel.getPlayerA();
        String[] playerB = gameModel.getPlayerB();

        int aScore = playerScore(playerA);
        int bScore = playerScore(playerB);
        System.out.println(Arrays.toString(playerA));
        System.out.println(Arrays.toString(playerB));

        if (aScore > 21 && bScore > 21) {
//            No clear winner
        } else {
            if (aScore == bScore) {
//            More analysis on the single value card
                winner = processTies(playerA, playerB);

            } else {
                if (aScore > 21 && bScore < 21) {
//                B wins
                    winner = "B";
                } else if (bScore > 21 && aScore < 21) {
//                A Wins
                    winner = "A";
                }
            }
        }


        System.out.println("A Score " + aScore);
        System.out.println("B Score " + bScore);
        System.out.println(winner);
        System.out.println("Received Winner : "+gameModel.getPlayerAWins());
        System.out.println("\n===============================================\n\n");

    }

    private static String processTies(String[] playerA, String[] playerB) {
        String winner = "";
        winnerLabel:
        for (int i = 0; i < playerA.length; i++) {
            for (int j = 0; j < playerB.length; j++) {
                if (i != j) {
//                    Skip as we are only comparing equal placed single value cards
                    continue;
                } else {
//                    Looking at the same positions
                    String valueA = playerA[i];
                    String valueB = playerB[j];
                    String aNum = removeLastChar(valueA);
                    String bNum = removeLastChar(valueB);

                    if (NumberUtils.isNumber(aNum) && NumberUtils.isNumber(bNum)) {
//                        All are numbers thus we can compare right away
                        int aNumValue = Integer.parseInt(aNum);
                        int bNumValue = Integer.parseInt(bNum);
                        if (aNumValue == bNumValue) {
//                            Check the second highest card
                            continue;

                        } else {
                            if (aNumValue > bNumValue) {
//                            A wins
                                winner = "A";
                                break winnerLabel;
                            } else {
//                                B wins
                                winner = "B";
                                break winnerLabel;
                            }
                        }

                    } else {
//                        not all are numbers, thus we need to evaluate the letters
                        int cleanA = cleanNumbers(aNum);
                        int cleanB = cleanNumbers(bNum);
                        if (cleanA == cleanB) {
//                            Check the third highest card
                            continue;

                        } else {
                            if (cleanA > cleanB) {
//                            A wins
                                winner = "A";
                                break winnerLabel;
                            } else {
//                                B wins
                                winner = "B";
                                break winnerLabel;
                            }
                        }

                    }

                }
            }

        }
        return winner;

    }

    private static int cleanNumbers(String s) {
        int value = 0;
        if (NumberUtils.isNumber(s)) {
            value = Integer.parseInt(s);
        } else {
            switch (s) {
                case "K":
                case "Q":
                case "J": {
                    value += 10;
                    break;
                }
                case "A": {
                    value += 11;
                }
            }
        }
        return value;

    }


    public static int playerScore(String[] player) {
        int runningTotal = 0;
        for (String r : player) {
            String pointValue = removeLastChar(r);
            if (NumberUtils.isNumber(pointValue)) {
//                Assign the value
                runningTotal += Integer.parseInt(pointValue);
            } else {
//                not a number thus process
                switch (pointValue) {
                    case "K":
                    case "Q":
                    case "J": {
                        runningTotal += 10;
                        break;
                    }
                    case "A": {
                        runningTotal += 11;
                    }
                }
            }
        }
        return runningTotal;

    }

    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

}

