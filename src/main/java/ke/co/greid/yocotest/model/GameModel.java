package ke.co.greid.yocotest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameModel {
    private String[] playerA;

    private String[] playerB;

    private String playerAWins;

    public String[] getPlayerA() {
        return playerA;
    }

    public void setPlayerA(String[] playerA) {
        this.playerA = playerA;
    }

    public String[] getPlayerB() {
        return playerB;
    }

    public void setPlayerB(String[] playerB) {
        this.playerB = playerB;
    }

    public String getPlayerAWins() {
        return playerAWins;
    }

    public void setPlayerAWins(String playerAWins) {
        this.playerAWins = playerAWins;
    }


    @Override
    public String toString() {
        return "GameModel{" +
                "playerA=" + Arrays.toString(playerA) +
                ", playerB=" + Arrays.toString(playerB) +
                ", playerAWins='" + playerAWins + '\'' +
                '}';
    }
}